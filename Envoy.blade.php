@servers(['web' => ['root@165.227.239.27']])

@task('dploy', ['on' => 'web', 'confirm' => true])
    cd /facebook-project
    git pull origin develop
    cd docker
    docker-compose run --rm php composer install
@endtask

@task('db-update', ['on' => 'web'])
    cd /facebook-project/docker
    docker-compose run --rm php bin/console doctrine:schema:update --force
@endtask

@task('assets', ['on' => 'web'])
    cd /facebook-project/docker
    docker-compose run --rm php yarn run encore dev
@endtask

