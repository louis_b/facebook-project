<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=150, unique=true, nullable=false)
     */
    private $facebookId;
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Portfolio", cascade={"persist"})
     * @ORM\JoinColumn(name="portfolio", referencedColumnName="id")
     */
    protected $portfolio;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Album", mappedBy="owner", cascade={"persist"})
     */
    protected $albums;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Configuration", mappedBy="user", cascade={"persist"})
     */
    protected $configurations;
    /**
     * @ORM\Column(type="string", length=50, unique=false, nullable=false)
     */
    protected $firstName;
    /**
     * @ORM\Column(type="string", length=50, unique=false, nullable=false)
     */
    protected $lastName;
    /**
     * @ORM\Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $email;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     */
    protected $isActive;

    public function __construct($facebookId, $firstName, $lastName, $email, $username)
    {
        $this->facebookId = $facebookId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->username = $username;
        $this->isActive = true;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        // use bcrypt so the return is not used
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->firstName,
            $this->lastName,
            $this->isActive
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->firstName,
            $this->lastName,
            $this->isActive
            ) = unserialize($serialized);
    }

    // AdvancedUserInterface methods
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    // personal methods

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName() : string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return User
     */
    public function setPortfolio(Portfolio $portfolio) : User
    {
        $this->portfolio = $portfolio;

        return $this;
    }

    /**
     * @return Portfolio
     */
    public function getPortfolio() : Portfolio
    {
        return $this->portfolio;
    }

    /**
     * @return Album[] | PersistentCollection
     */
    public function getAlbums()
    {
        return $this->albums;
    }

    /**
     * @return Configuration[] | PersistentCollection
     */
    public function getConfigurations()
    {
        return $this->configurations;
    }
}
