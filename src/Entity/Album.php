<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 */
class Album
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=150, unique=true, nullable=false)
     */
    private $facebookId;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    protected $owner;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     */
    protected $isActive;
    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $description;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}
