<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 */
class Configuration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(name="configuration_key", type="string", length=50, nullable=false)
     */
    protected $key;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="configuration_user", referencedColumnName="id")
     */
    protected $user;
    /**
     * @ORM\Column(name="configuration_value", type="string", length=2000, nullable=true)
     */
    protected $value;
    /**
     * @ORM\Column(name="configuration_updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * Configuration constructor.
     * @param User $user
     * @param string $key
     * @param string $value
     */
    public function __construct(User $user, string $key, string $value)
    {
        $this->user = $user;
        $this->key = $key;
        $this->value = $value;
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @param $value
     *
     * @return Configuration
     */
    public function setValue($value) : Configuration
    {
        $this->value = $value;

        return $this;
    }
}
