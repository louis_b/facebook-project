<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PortfolioRepository")
 */
class Portfolio
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    protected $owner;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Theme", cascade={"persist"})
     * @ORM\JoinColumn(name="theme", referencedColumnName="id")
     */
    protected $theme;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     */
    protected $isActive;
    /**
     * @ORM\Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $urlKey;
    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=false)
     */
    protected $title;
    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    protected $subtitle;
    /**
     * @ORM\Column(type="string", length=5000, unique=false, nullable=true)
     */
    protected $description;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    protected $isContactEnable;
    /**
     * @ORM\Column(type="string", length=100, unique=false, nullable=false)
     */
    protected $contactEmail;
    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=false)
     */
    protected $contactTitle;
    /**
     * @ORM\Column(type="string", length=5000, unique=false, nullable=true)
     */
    protected $contactDescription;

    /**
     * Portfolio constructor.
     * @param string $urlKey
     * @param string $title
     * @param string $description
     * @param string $contactEmail
     */
    public function __construct(string $urlKey, string $title, string $contactEmail = '', string $description = '')
    {
        $this->urlKey = $urlKey;
        $this->title = $title;
        $this->description = $description;
        $this->contactEmail = $contactEmail;
        $this->isActive = true;
        $this->isContactEnable = false;
        $this->subtitle = '';
        $this->contactTitle = 'Contactez moi';
        $this->contactDescription = '';
    }

    /**
     * @param User $owner
     *
     * @return Portfolio
     */
    public function setOwner(User $owner) : Portfolio
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @param Theme $theme
     *
     * @return Portfolio
     */
    public function setTheme(Theme $theme) : Portfolio
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @param string $title
     *
     * @return Portfolio
     */
    public function setTitle(string $title) : Portfolio
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSubtitle() : string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     *
     * @return Portfolio
     */
    public function setSubtitle(string $subtitle) : Portfolio
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return Portfolio
     */
    public function setDescription(string $description) : Portfolio
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getUrlKey() : string
    {
        return $this->urlKey;
    }

    /**
     * @return User
     */
    public function getOwner() : User
    {
        return $this->owner;
    }

    /**
     * @return Theme
     */
    public function getTheme() : Theme
    {
        return $this->theme;
    }

    /**
     * @return bool
     */
    public function isContactEnable() : bool
    {
        return $this->isContactEnable;
    }

    /**
     * @param bool $isEnable
     *
     * @return Portfolio
     */
    public function setIsContactEnable(bool $isEnable) : Portfolio
    {
        $this->isContactEnable = $isEnable;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactEmail() : string
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     *
     * @return Portfolio
     */
    public function setContactEmail(string $contactEmail) : Portfolio
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactTitle() : string
    {
        return $this->contactTitle;
    }

    /**
     * @param string $contactTitle
     *
     * @return Portfolio
     */
    public function setContactTitle(string $contactTitle): Portfolio
    {
        $this->contactTitle = $contactTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactDescription() : string
    {
        return $this->contactDescription;
    }

    /**
     * @param string $contactDescription
     *
     * @return Portfolio
     */
    public function setContactDescription(string $contactDescription): Portfolio
    {
        $this->contactDescription = $contactDescription;

        return $this;
    }
}
