<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhotoRepository")
 */
class Photo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=150, unique=true, nullable=false)
     */
    private $facebookId;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Album", cascade={"persist"})
     * @ORM\JoinColumn(name="album", referencedColumnName="id")
     */
    protected $album;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     */
    protected $isActive;
    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=500, nullable=false)
     */
    protected $src;
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $description;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;
}
