<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThemeRepository")
 */
class Theme
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique=true, nullable=false)
     */
    private $code;
    /**
     * @ORM\Column(type="string", length=50, unique=false, nullable=false)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=150, unique=false, nullable=false)
     */
    protected $previewSrc;
    /**
     * @ORM\Column(type="string", length=50, unique=false, nullable=true)
     */
    protected $fontFamily;
    /**
     * @ORM\Column(type="integer", nullable=true, options={"default":16})
     */
    protected $fontSize;
    /**
     * @ORM\Column(type="string", length=20, unique=false, nullable=true)
     */
    protected $fontWeight;
    /**
     * @ORM\Column(type="string", length=40, unique=false, nullable=true)
     */
    protected $mainColor;
    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    protected $isDark;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Portfolio", cascade={"persist"})
     * @ORM\JoinColumn(name="portfolio", referencedColumnName="id")
     */
    protected $portfolio;

    /**
     * Theme constructor.
     * @param string $code
     * @param string $name
     * @param string $previewSrc
     * @param Portfolio $portfolio
     */
    public function __construct(string $code, string $name, string $previewSrc, Portfolio $portfolio)
    {
        $this->code = $code;
        $this->name = $name;
        $this->previewSrc = $previewSrc;
        $this->portfolio = $portfolio;
        $this->fontFamily = '';
        $this->mainColor = '#000000';
        $this->isDark = false;
    }

    /**
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getFontFamily() : string
    {
        return $this->fontFamily;
    }

    /**
     * @param string $fontFamily
     *
     * @return Theme
     */
    public function setFontFamily(string $fontFamily): Theme
    {
        $this->fontFamily = $fontFamily;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainColor() : string
    {
        return $this->mainColor;
    }

    /**
     * @param string $mainColor
     *
     * @return Theme
     */
    public function setMainColor(string $mainColor): Theme
    {
        $this->mainColor = $mainColor;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDark() : bool
    {
        return $this->isDark;
    }

    /**
     * @param bool $isDark
     *
     * @return Theme
     */
    public function setIsDark(bool $isDark): Theme
    {
        $this->isDark = $isDark;

        return $this;
    }
}
