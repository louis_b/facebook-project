<?php

namespace App\Handler;

use App\Service\Facebook;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;

class FacebookLogoutHandler implements LogoutSuccessHandlerInterface
{
    /**
     * @var SessionInterface $session
     */
    private $session;
    /**
     * @var Facebook $fb
     */
    private $fb;

    public function __construct(SessionInterface $session, Facebook $fb)
    {
        $this->session = $session;
        $this->fb = $fb;
    }

    public function onLogoutSuccess(Request $request)
    {
        $logoutUrl = $this->fb->getLogoutUrl();
        $this->session->clear();

        return new RedirectResponse($logoutUrl);
    }
}