<?php

namespace App\Repository;

use App\Entity\Theme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ThemeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Theme::class);
    }

    /**
     * @param Theme $theme
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(Theme $theme)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($theme);
        $entityManager->flush();
    }

    /**
     * @param string $value
     *
     * @return Theme|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByCode(string $value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.code = :value')
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
