<?php

namespace App\Repository;

use App\Entity\Portfolio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PortfolioRepository extends ServiceEntityRepository
{

    /**
     * PortfolioRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Portfolio::class);
    }

    /**
     * @param $urlKey
     *
     * @return Portfolio|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByUrlKey($urlKey)
    {
        return $this->createQueryBuilder('p')
            ->where('p.urlKey = :value')
            ->setParameter('value', $urlKey)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Portfolio $portfolio
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Portfolio $portfolio)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();
        $entityManager->persist($portfolio);
        $entityManager->flush();
    }
}
