<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Configuration;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Configuration::class);
    }

    /**
     * Update specific configurations
     *
     * @param array $configurations
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function massUpdate(array $configurations)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();
        foreach ($configurations as $configuration) {
            $entityManager->persist($configuration);
        }

        $entityManager->flush();
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('c', 'c.key')
            ->where('c.user = :value')->setParameter('value', $user->getId())
            ->orderBy('c.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
