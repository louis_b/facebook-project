<?php

namespace App\Controller;

use App\Entity\Portfolio;
use App\Repository\ConfigurationRepository;
use App\Repository\PortfolioRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends Controller
{

    /**
     * @Route("/{urlKey}/portfolio", name="user_portfolio")
     *
     * @param string $urlKey
     * @param PortfolioRepository $portfolioRepository
     * @param ConfigurationRepository $configurationRepository
     *
     * @return Response|RedirectResponse
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index(string $urlKey, PortfolioRepository $portfolioRepository, ConfigurationRepository $configurationRepository)
    {
        /** @var Portfolio $portfolio */
        $portfolio = $portfolioRepository->findByUrlKey($urlKey);

        // if no portfolio is associated to the current url key
        if (is_null($portfolio)) {
            return new RedirectResponse($this->generateUrl('index'));
        }

        return $this->render('frontend/' . $portfolio->getTheme()->getCode() . '/portfolio.html.twig', [
            'portfolio' => $portfolio,
            'configurations' => $configurationRepository->findByUser($portfolio->getOwner()),
        ]);
    }
}
