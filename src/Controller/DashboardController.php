<?php

namespace App\Controller;

use App\Entity\Portfolio;
use App\Entity\Theme;
use App\Form\PortfolioType;
use App\Form\SeoType;
use App\Form\ThemeType;
use App\Repository\PortfolioRepository;
use App\Service\Seo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{

    /**
     * @Route("/dashboard", name="dashboard_home")
     *
     * @return Response
     */
    public function home()
    {
        return $this->render('dashboard/index.html.twig');
    }

    /**
     * @Route("/dashboard/style", name="dashboard_style")
     *
     * @return Response
     */
    public function style()
    {
        /** @var Theme $theme */
        $theme = $this->getUser()->getPortfolio()->getTheme();
        /** @var Form $themeForm */
        $themeForm = $this->createForm(ThemeType::class, $theme);

        return $this->render('dashboard/style.html.twig', [
            'themeForm' => $themeForm->createView(),
        ]);
    }

    /**
     * @Route("/dashboard/albums", name="dashboard_albums")
     *
     * @return Response
     */
    public function albums()
    {
        return $this->render('dashboard/albums.html.twig');
    }

    /**
     * @Route("/dashboard/seo", name="dashboard_seo")
     *
     * @param Request $request
     * @param Seo $seoService
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seo(Request $request, Seo $seoService)
    {
        /** @var Form $seoForm */
        $seoForm = $this->createForm(SeoType::class);

        $seoForm->handleRequest($request);
        if (!$seoForm->isSubmitted()) {
            $seoForm->setData($seoService->getSeoConfigurationsValues($this->getUser()));
        }
        if ($seoForm->isSubmitted() && $seoForm->isValid()) {
            /** @var  $formData */
            $formData = $seoForm->getData();
            $seoService->updateSeoConfigurations($formData['seo_title'], $formData['seo_description'], $formData['google_analytics']);
        }

        return $this->render('dashboard/seo.html.twig', [
            'seoForm' => $seoForm->createView(),
        ]);
    }

    /**
     * @Route("/dashboard/settings", name="dashboard_settings")
     *
     * @param Request $request
     * @param PortfolioRepository $portfolioRepository
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function settings(Request $request, PortfolioRepository $portfolioRepository)
    {
        /** @var Portfolio $portfolio */
        $portfolio = $this->getUser()->getPortfolio();
        /** @var Form $portfolioForm */
        $portfolioForm = $this->createForm(PortfolioType::class, $portfolio);

        $portfolioForm->handleRequest($request);
        if ($portfolioForm->isSubmitted() && $portfolioForm->isValid()) {
            $portfolioRepository->update($portfolio);
        }

        return $this->render('dashboard/settings.html.twig', [
            'portfolioForm' => $portfolioForm->createView(),
        ]);
    }
}
