<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Service\Facebook;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class AppController extends Controller
{

    /**
     * @Route("/", name="index")
     *
     * @param Facebook $fb
     *
     * @return Response
     */
    public function index(Facebook $fb)
    {
        /** @var string $loginUrl */
        $loginUrl = $fb->getLoginUrl();

        return $this->render('pages/welcome.html.twig', ['loginUrl' => $loginUrl]);
    }

    /**
     * @Route("/login", name="fb_login_after")
     *
     * @param Facebook $fb
     *
     * @return RedirectResponse
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function login(Facebook $fb)
    {
        $fb->connect();

        return new RedirectResponse($this->generateUrl('dashboard_home'));
    }
}