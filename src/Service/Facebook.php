<?php

namespace App\Service;

use App\Entity\Portfolio;
use App\Entity\User;
use \App\Service\User as UserService;
use \App\Service\Portfolio as PortfolioService;
use \App\Service\Theme as ThemeService;
use \App\Service\Seo as SeoService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\GraphNodes\GraphUser;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class Facebook extends \Facebook\Facebook
{
    /** @var ContainerInterface $container */
    protected $container;
    /** @var SessionInterface $session */
    protected $session;
    /** @var $requestStack */
    protected $requestStack;
    /** @var EntityManager $entityManager */
    protected $entityManager;
    /** @var UserRepository $userRepository */
    protected $userRepository;
    /** @var UrlGeneratorInterface $router */
    protected $router;
    /** @var UserService $userService */
    protected $userService;
    /** @var PortfolioService $portfolioService */
    protected $portfolioService;
    /** @var ThemeService $themeService */
    protected $themeService;
    /** @var SeoService $seoService */
    protected $seoService;
    /** @var $appId */
    private $appId;

    /**
     * Facebook constructor.
     * @param ContainerInterface $container
     * @param SessionInterface $session
     * @param RequestStack $requestStack
     * @param EntityManagerInterface $entityManager
     * @param UrlGeneratorInterface $router
     * @param UserService $userService
     * @param PortfolioService $portfolioService
     * @param ThemeService $themeService
     * @param SeoService $seoService
     *
     * @throws FacebookSDKException
     */
    public function __construct(
        ContainerInterface $container,
        SessionInterface $session,
        RequestStack $requestStack,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $router,
        UserService $userService,
        PortfolioService $portfolioService,
        ThemeService $themeService,
        SeoService $seoService
    ) {
        $this->container = $container;
        $this->session = $session;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->userRepository = $entityManager->getRepository(User::class);
        $this->router = $router;
        $this->userService = $userService;
        $this->portfolioService = $portfolioService;
        $this->themeService = $themeService;
        $this->seoService = $seoService;
        /** @var mixed[] $config */
        $config = [
            'app_id' => $this->getAppId(),
            'app_secret' => $container->getParameter('fb.appKey'),
            'default_graph_version' => $container->getParameter('fb.graphVersion'),
        ];

        parent::__construct($config);
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId = $this->container->getParameter('fb.appId');
    }

    /**
     * @return string
     */
    public function getLoginUrl(){
        /** @var FacebookRedirectLoginHelper $helper */
        $helper = $this->getRedirectLoginHelper();
        /** @var string[] $permissions */
        $permissions = ['email'];
        /** @var string $loginUrl */
        $loginUrl = $this->router->generate('fb_login_after', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $loginUrl = $helper->getLoginUrl($loginUrl, $permissions);

        return $loginUrl;
    }

    /**
     * @return string
     */
    public function getLogoutUrl()
    {
        /** @var string $route */
        $route = $this->container->getParameter('fb.logout.after.route');
        /** @var string $pattern */
        $pattern = $this->container->getParameter('fb.logout.pattern');
        $pattern = str_replace('{redirect_url}', $this->router->generate($route, [], UrlGeneratorInterface::ABSOLUTE_URL), $pattern);
        $pattern = str_replace('{access_token}', $this->session->get('fb_access_token'), $pattern);

        return $pattern;
    }

    /**
     * @throws FacebookSDKException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function connect()
    {
        /** @var FacebookRedirectLoginHelper $helper */
        $helper = $this->getRedirectLoginHelper();

        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }

        try {
            $accessToken = $helper->getAccessToken();
        } catch(FacebookSDKException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $this->getOAuth2Client();
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId((string) $this->getAppId()); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }
        }

        // manage use connection
        $this->session->set('fb_access_token', (string) $accessToken);
        $this->connectSymfonyUser();
    }

    /**
     * Connect symfony user associated to the facebook one
     *
     * @throws FacebookSDKException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function connectSymfonyUser()
    {
        /** @var GraphUser $graphUser */
        $graphUser = $this->get('/me?fields=id,first_name,last_name,email', $this->session->get('fb_access_token'))->getGraphUser();
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['facebookId' => $graphUser->getId()]);
        if (!$user instanceof User) {
            $user = $this->initializeNewUser($graphUser);
        }

        // The third parameter "main" is the firewall in security.yml
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);

        // main is also the firewall name here
        $this->container->get('session')->set('_security_main', serialize($token));

        // Fire the login event manually
        $event = new InteractiveLoginEvent($this->requestStack->getCurrentRequest(), $token);
        $this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }

    /**
     * @param GraphUser $graphUser
     *
     * @return User
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function initializeNewUser(GraphUser $graphUser) : User
    {
        /** @var Portfolio $portfolio */
        $portfolio = new Portfolio(
            $this->portfolioService->getDefaultUrlKey($graphUser->getFirstName(), $graphUser->getLastName()),
            $this->portfolioService->getDefaultTitle($graphUser->getFirstName()),
            $graphUser->getEmail()
        );
        $portfolio->setTheme($this->themeService->createDefaultTheme($portfolio));
        /** @var User $user */
        $user = new User(
            $graphUser->getId(),
            $graphUser->getFirstName(),
            $graphUser->getLastName(),
            $graphUser->getEmail(),
            $this->userService->generateUsername($graphUser->getFirstName(), $graphUser->getLastName())
        );
        $user->setPassword($this->userService->getRandomPassword($user));
        $user->setPortfolio($portfolio->setOwner($user));

        $this->userRepository->create($user);
        $this->seoService->initSeoConfigurations($user);

        return $user;
    }
}