<?php

namespace App\Service;

use App\Entity\Theme as ThemeEntity;
use App\Repository\ThemeRepository;

class Theme
{

    /**
     * @var string DEFAULT_THEME_CODE
     */
    const DEFAULT_THEME_CODE = 'default';
    /**
     * @var string DEFAULT_THEME_CODE
     */
    const DEFAULT_THEME_NAME = 'Default Theme';
    /**
     * @var string DEFAULT_THEME_PREVIEW_SRC
     */
    const DEFAULT_THEME_PREVIEW_SRC = '';

    /**
     * @var ThemeRepository $themeRepository
     */
    protected $themeRepository;

    /**
     * Theme constructor.
     * @param ThemeRepository $themeRepository
     */
    public function __construct(ThemeRepository $themeRepository)
    {
        $this->themeRepository = $themeRepository;
    }

    /**
     * @param \App\Entity\Portfolio $portfolio
     *
     * @return ThemeEntity
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createDefaultTheme(\App\Entity\Portfolio $portfolio)
    {
        /** @var ThemeEntity $theme */
        $theme = new ThemeEntity(self::DEFAULT_THEME_CODE, self::DEFAULT_THEME_NAME, self::DEFAULT_THEME_PREVIEW_SRC, $portfolio);
        $this->themeRepository->create($theme);

        return $theme;
    }
}
