<?php

namespace App\Service;

use App\Entity\User as UserEntity;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class User
{

    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    protected $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     *
     * @return string
     */
    public function generateUsername($firstName, $lastName)
    {
        /** @var string $username */
        $username = htmlentities($firstName.$lastName, ENT_NOQUOTES);
        $username = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $username);
        $username = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $username);
        $username = preg_replace('#&[^;]+;#', '', $username);
        $username = strtolower($username);

        return $username . '.' . substr(uniqid(), 1, 6);
    }

    /**
     * @param UserEntity $user
     * @param int $length
     *
     * @return bool|string
     */
    public function getRandomPassword(UserEntity $user, $length = 11)
    {
        /** @var string $password */
        $password = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr(str_shuffle($password), 0, $length);
        $password = $this->encoder->encodePassword($user, $password);

        return $password;
    }
}
