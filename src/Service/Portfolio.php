<?php

namespace App\Service;

class Portfolio
{

    /**
     * @param string $userName
     *
     * @return string
     */
    public function getDefaultTitle($userName)
    {
        return $userName . "'s Portfolio";
    }

    /**
     * @param string $userFirstName
     * @param string $userLastName
     *
     * @return string
     */
    public function getDefaultUrlKey($userFirstName, $userLastName)
    {
        /** @var string $urlKey */
        $urlKey = htmlentities($userFirstName.$userLastName, ENT_NOQUOTES);
        $urlKey = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $urlKey);
        $urlKey = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $urlKey);
        $urlKey = preg_replace('#&[^;]+;#', '', $urlKey);
        $urlKey = strtolower($urlKey);

        return $urlKey . '-' . substr(uniqid(), 1, 3);
    }
}
