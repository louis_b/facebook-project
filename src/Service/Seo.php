<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Configuration;
use App\Entity\User as UserEntity;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Seo
{

    /**
     * @var ConfigurationRepository $configurationRepository
     */
    private $configurationRepository;
    /**
     * @var \App\Entity\User $user
     */
    private $user;

    /**
     * Seo constructor.
     * @param ConfigurationRepository $configurationRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ConfigurationRepository $configurationRepository, TokenStorageInterface $tokenStorage)
    {
        $this->configurationRepository = $configurationRepository;
        if (!is_null($tokenStorage->getToken())) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }

    /**
     * @param UserEntity $user
     *
     * @return array
     */
    public function getSeoConfigurations(UserEntity $user) : array
    {
        return [
            'seo_title' => $this->configurationRepository->findOneBy([
                'key' => 'seo_title',
                'user' => $user,
            ]),
            'seo_description' => $this->configurationRepository->findOneBy([
                'key' => 'seo_description',
                'user' => $user,
            ]),
            'google_analytics' => $this->configurationRepository->findOneBy([
                'key' => 'google_analytics',
                'user' => $user,
            ]),
        ];
    }

    /**
     * @return array
     */
    public function getSeoConfigurationsValues(UserEntity $user) : array
    {
        /** @var array $configurations */
        $configurations = [];
        /**
         * @var string $key
         * @var Configuration $configuration
         */
        foreach ($this->getSeoConfigurations($user) as $key => $configuration) {
            if ($configuration instanceof Configuration) {
                $configurations[$key] = $configuration->getValue();
            }
        }

        return $configurations;
    }

    /**
     * @param UserEntity $user
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function initSeoConfigurations(UserEntity $user)
    {
        /** @var string[] $configurations */
        $configurations = [];
        $configurations['seo_title'] = new Configuration($user, 'seo_title', $user->getPortfolio()->getTitle());
        $configurations['seo_description'] = new Configuration($user, 'seo_description', $user->getPortfolio()->getDescription());
        $configurations['google_analytics'] = new Configuration($user, 'google_analytics', '');

        $this->configurationRepository->massUpdate($configurations);
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $analytics
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateSeoConfigurations(string $title, string $description, string $analytics) : void
    {
        /** @var Configuration[] $configurations */
        $configurations = $this->getSeoConfigurations($this->user);
        if (!$configurations['seo_title'] instanceof Configuration) {
            $configurations['seo_title'] = new Configuration($this->user, 'seo_title', $title);
        }
        $configurations['seo_title']->setValue($title);

        if (!$configurations['seo_description'] instanceof Configuration) {
            $configurations['seo_description'] = new Configuration($this->user, 'seo_description', $description);
        }
        $configurations['seo_description']->setValue($description);

        if (!$configurations['google_analytics'] instanceof Configuration) {
            $configurations['google_analytics'] = new Configuration($this->user, 'google_analytics', $analytics);
        }
        $configurations['google_analytics']->setValue($analytics);

        $this->configurationRepository->massUpdate($configurations);
    }
}
