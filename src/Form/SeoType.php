<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seo_title', TextType::class, [
                'label' => 'Titre de la page',
                'attr' =>  ['class' => 'form-control'],
            ])
            ->add('seo_description', TextareaType::class, [
                'label' => 'Description de la page',
                'attr' =>  [
                    'class' => 'form-control-no-height',
                    'rows' => '3',
                ],
            ])
            ->add('google_analytics', TextareaType::class, [
                'label' => 'Script de tracking Google Analytics',
                'required'   => false,
                'empty_data' => '',
                'attr' =>  [
                    'class' => 'form-control-no-height',
                    'rows' => '5',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Sauvegarder la configuration SEO',
                'attr' =>  ['class' => 'btn btn-info btn-fill pull-right'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
