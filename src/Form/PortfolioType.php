<?php

namespace App\Form;

use App\Entity\Portfolio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PortfolioType
 * @package App\Form
 */
class PortfolioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre principale',
                'attr' =>  ['class' => 'form-control'],
            ])
            ->add('subtitle', TextType::class, [
                'label' => 'Sous titre',
                'required' => false,
                'empty_data' => '',
                'attr' =>  ['class' => 'form-control'],
            ])
            ->add('description', TextareaType::class, [
                'required'   => false,
                'empty_data' => '',
                'attr' =>  [
                    'class' => 'form-control-no-height',
                    'rows' => '5',
                ],
            ])
            ->add('is_contact_enable', CheckboxType::class, [
                'label' => 'Autoriser la page de contact',
                'required' => false,
            ])
            ->add('contact_email', EmailType::class, [
                'label' => 'Adresse Email de contact',
                'attr' =>  [
                    'class' => 'form-control',
                    'placeholder' => 'example@gmail.com',
                ],
            ])
            ->add('contact_title', TextType::class, [
                'label' => 'Titre de la page de contact',
                'attr' =>  ['class' => 'form-control'],
            ])
            ->add('contact_description', TextareaType::class, [
                'label' => 'Description de la page de contact',
                'required'   => false,
                'empty_data' => '',
                'attr' =>  [
                    'class' => 'form-control-no-height',
                    'rows' => '4',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Sauvegarder le portfolio',
                'attr' =>  ['class' => 'btn btn-info btn-fill pull-right'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Portfolio::class,
        ]);
    }
}
