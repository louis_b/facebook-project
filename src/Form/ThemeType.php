<?php

namespace App\Form;

use App\Entity\Theme;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ThemeType
 * @package App\Form
 */
class ThemeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, [
                'label' => 'Thème global',
                'attr' =>  ['class' => 'form-control'],
            ])
            ->add('main_color', TextType::class, [
                'label' => 'Couleur principale',
                'attr' =>  ['class' => 'form-control'],
            ])
            ->add('is_dark', CheckboxType::class, [
                'label' => 'Version Light / Dark',
                'required' => false,
            ])
            ->add('font_family', TextType::class, [
                'label' => 'Police d\'écriture',
                'attr' =>  ['class' => 'form-control',],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Sauvegarder le thème',
                'attr' =>  ['class' => 'btn btn-info btn-fill pull-right'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Theme::class,
        ]);
    }
}
