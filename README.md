Facebook project
==============

First, clone this repository:

```bash
$ git clone git@gitlab.com:louis_b/facebook-project.git
```

Run `composer install` and `npm i`

Then, run in the `docker` folder:

```bash
$ docker-compose up --build
```

Add `fb.project.dev` in your `etc/hosts` file.

You are done! 
You can visit your Symfony application on the following URL: http://symfony.dev

### Webpack commands

```bash
#compile assets
$ npm run dev 

#watch assets
$ npm run watch

# production 
$ npm run prod
```

### Doctrine commands
Enter into php container in order to run doctrine command :
```bash
# enter into container
$ docker-compose exec php sh
```